// Go: SLOC=11 LLOC=4
package main

import "fmt"

func main() {
	foo := `//  might
/*  consider
"   comments
*/  and
"   quotes
`
	fmt.Printf("%s\n", foo)
}
